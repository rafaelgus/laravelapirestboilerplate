<?php




Route::get('/', function () {
    return view('welcome');
});

Route::get('user/verify/{verification_code}', 'Api\AuthController@verifyUser');


Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.request');

Route::post('password/reset', 'Auth\ResetPasswordController@postReset')->name('password.reset');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
