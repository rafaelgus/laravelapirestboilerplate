<?php


 /* Registro  */
Route::post('register', 'Api\AuthController@register');

/* login */
Route::post('login', 'Api\AuthController@login');

/* Recuperar pass */

Route::post('recover', 'Api\AuthController@recover');
Route::post('refresh', 'AuthController@refresh');
Route::post('me', 'AuthController@me');


Route::group(['middleware' => ['jwt.auth']], function() {

    Route::get('logout', 'Api\AuthController@logout');

    /* Solo pruebas */

    Route::get('test', function(){
        return response()->json(['foo'=>'bar']);
    });
});
