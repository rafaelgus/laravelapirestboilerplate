## Laravel API Boilerplate (JWT Edition) para  Laravel 5.8


Laravel API Boilerplate es un "kit de inicio" que puede utilizar para crear su primera API en segundos. Como puedes imaginar, está construido sobre el  Laravel Framework. Esta versión está construida en Laravel 5.8!

Está construido sobre estas dos librerias:

* JWT-Auth - [tymondesigns/jwt-auth](https://github.com/tymondesigns/jwt-auth)

* Laravel-CORS [barryvdh/laravel-cors](http://github.com/barryvdh/laravel-cors)

Lo que hice es realmente simple: una integración de estos dos paquetes y una configuración de algunos métodos de autenticación y recuperación de credenciales.

## Instalacion

1. Clona o copia el proyecto a tu entorno local.
2. Entra en el proyecto clonado cd carpeta de proyecto.
3. Ejecuta composer install.
4. Ejecuta npm install.
5. Configura tu archivo .env con tus datos personales.
6. DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=tu base de datos
DB_USERNAME=tu usuario 
DB_PASSWORD=tu contraseña de base de datos.
7. MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=tus datos
MAIL_PASSWORD=tus datos
MAIL_FROM_ADDRESS=tus datos
MAIL_FROM_NAME=tus datos
MAIL_ENCRYPTION=null

Una vez que se complete el procedimiento de creación del proyecto, ejecute el comando `php artisan migrate` para instalar las tablas requeridas.



## Principales características

### Controladores de autenticación listos para usar

Ya no tiene que preocuparse por la autenticación y la recuperación de la contraseña. Creé cuatro controladores que puedes encontrar en `App\ Controllers\Api\AuthController` para esas operaciones.



* `POST api/auth/login`, para hacer el inicio de sesión y obtener su token de acceso;
  
* `POST api/auth/refresh`, para actualizar un token de acceso existente obteniendo uno nuevo;


* `POST api/auth/signup`, para crear un nuevo usuario en su aplicación;
* 
* `POST api/auth/recovery`, para recuperar tus credenciales;
* 
* `POST api/auth/reset`, para restablecer su contraseña después de la recuperación;
* 
* `POST api/auth/logout`, para cerrar sesión en el usuario invalidando el token pasado;
* 
* `GET api/auth/me`, para obtener datos del usuario actual;

### Archivo separado para rutas

Todas las rutas API se pueden encontrar en el archivo `route / api.php`. Esto también sigue la convención de Laravel 5.8.

###  Generacion de Secrets 

Cada vez que cree un nuevo proyecto a partir de este repositorio, se ejecutará el comando _php artisan jwt: generate_.



## Cross Origin Resource Sharing

Si desea habilitar CORS para una ruta o grupo de rutas específico, solo tiene que usar el middleware _cors_ en ellos.

Gracias al paquete _barryvdh / laravel-cors_, puede manejar CORS fácilmente. Para obtener más información, consulte <a href="https://github.com/barryvdh/laravel-cors" target="_blank"> los documentos en esta página </a>.

## Tests

Si desea contribuir a este proyecto, siéntase libre de hacerlo y abra un RP. Sin embargo, asegúrese de tener pruebas para lo que implementa.

Para realizar pruebas:

* crea una base de datos `homestead_test` en tu máquina;


Si desea especificar un nombre diferente para la base de datos de prueba, no olvide cambiar el valor en el archivo `.env`.


